package main

import(
	"fmt"
	"bufio"
	"os"
	s "strings"
)


//Funcion que realizar operacion or
func orOperation(a int, b int) int {
	if a==1 && b==1{
		return 1
	}else if a==1 && b==0{
		return 1
	}else if a==0 && b==1{
		return 1
	}else{
		return 0
	}
}

//Funcion que realizar operacion and
func andOperation(a int, b int) int {
        if a==1 && b==1{
                return 1
        }else if a==1 && b==0{
                return 0
        }else if a==0 && b==1{
                return 0
        }else{
                return 0
        }
}

//Funcion que compara si dos arreglos de enteros son iguales
func igualdad(a []int, b[]int, n int)bool{
	var i int=0
	for i=0;i<n;i++{
		if(a[i]!=b[i]){
			return false
		}
	}

	return true
}

//opera en cada entrara del arrelo laoperacion or
func sumaOr(a []int, b []int, n int) []int{
	res := make([]int, n)
	var i int
	for i=0;i<n;i++{
		res[i] =orOperation(a[i],b[i])
	}
	return res
}

//opera en cada entrara del arrelo laoperacion and
func multiplicarAnd(a []int, b []int, n int) []int{
        res := make([]int, n)
        var i int
        for i=0;i<n;i++{
                res[i] =andOperation(a[i],b[i])
        }
        return res
}

//crea un cetor en cuya entrada k-esima tal que 0 <= k <= n, para que sea un vector unitario
func creaVector(k int,n int )[]int{
	val := make([]int, n)
	val[k] =1
	return val
}


//Esta funcion tiene como entrada un estrado y un symbolo el cual regresa un estado resultado de evaluar el estado.
func cambio_de_estado(q_s []int, palabra string, fun_trans map[string][]int, est_aux map[int]string, bol_fun map[string]bool)[]int{
	resultado := make([]int,len(q_s))
	var i int
	for i=0;i<len(q_s);i++{
		if q_s[i]==1{
			if bol_fun[est_aux[i]+":"+palabra]==true{
				resultado = sumaOr(fun_trans[est_aux[i]+":"+palabra],resultado, len(q_s))
			}
		}
	}
	return resultado
}

//Verifica si algunos de los estados de aceptacion concierda con los estados finales
func estado_exitoso(ultimo_estado []int, e_acep map[string][]int, e_acep_aux map[int]string)bool{
	var i int
	var prueba []int
	var prueba2 []int
	var result bool =false
	for i=0;i<len(e_acep);i++{
		prueba = multiplicarAnd(e_acep[e_acep_aux[i]],ultimo_estado,len(ultimo_estado))
		prueba2 =e_acep[e_acep_aux[i]]
		result = result || igualdad(prueba,prueba2,len(ultimo_estado))
	}
	return result
}

func main(){

	//Estructura del automata
	estados := make(map[string]int)
	estado_inicial:= make(map[string][]int)
	estados_de_aceptacion := make(map[string][]int)
	alfabeto := make(map[string]bool)
	ft := make(map[string][]int)

	//Estructuras auxiliares
	ft_aux := make(map[string]bool)
	estados_aux := make(map[int]string)
	var estado_inicial_string  string
	estados_de_aceptacion_auxiliar := make(map[int]string)


	var p1 []string
	var p2 []string

	//Se abre el archivo
	archivo, error :=os.Open("definition2.txt")
	if error!=nil{
		fmt.Println("Existe un error")
	}

	//El archivo se coloca en un buffer para ser escaneado
	scanner :=bufio.NewScanner(archivo)
	var i int
	var aux string
	var j_aux int = 0

	for scanner.Scan(){
		i++
		linea:= scanner.Text()

		//Seleccionar  estado
		if linea == "#states"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#initial"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#accepting"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#alphabet"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#transitions"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else{
		}


		var l int

		//Llena los parametros dependiento de la etapa
		if aux=="#states"{
			estados[linea] = i-1
			estados_aux[i-1] = linea
		}else if aux =="#initial"{
			estado_inicial[linea] = creaVector(estados[linea],len(estados))
			estado_inicial_string=linea
		}else if aux == "#accepting"{
			j_aux++
			vcero := make([]int, len(estados))
			vcero = sumaOr(vcero,creaVector(estados[linea],len(estados)), len(estados))
			estados_de_aceptacion[linea]=vcero
			estados_de_aceptacion_auxiliar[j_aux-1]=linea
		}else if aux =="#alphabet"{
			alfabeto[linea] = true
		}else if aux =="#transitions"{
			p1 = s.Split(linea,">")
			p2 = s.Split(p1[1],",")
			vcero := make([]int, len(estados))
			for l=0;l<len(p2);l++{
				vcero = sumaOr(vcero,creaVector(estados[p2[l]],len(estados)), len(estados))
			}
			ft[p1[0]] = vcero
			ft_aux[p1[0]] = true
		}
	}


	//Imprime en pantalla
	fmt.Println(" Estados")
	fmt.Println(estados)
	fmt.Println(" ")
	fmt.Println(" Estado inicial ")
	fmt.Println(estado_inicial)
	fmt.Println(" ")
	fmt.Println(" Estados de aceptacion ")
	fmt.Println(estados_de_aceptacion)
	fmt.Println(" ")
	fmt.Println(" Alfabeto ")
	fmt.Println(alfabeto)
	fmt.Println(" ")
	fmt.Println(" Transiciones ")
	fmt.Println(ft)

	fmt.Println("Pruebas para la transiciones de cadenas")

	//Lectura de la cadena
	var palabra string
	//palabra := "abcabcbabcabcba"
	fmt.Scanf("%s", &palabra)
	array_pal := s.Split(palabra, "")

	auxiliar_estado:=estado_inicial[estado_inicial_string]


	fmt.Println(auxiliar_estado)
	for i=0;i<len(array_pal);i++{
		auxiliar_estado=cambio_de_estado(auxiliar_estado,array_pal[i],ft,estados_aux,ft_aux)
		fmt.Println(auxiliar_estado, array_pal[i])
	}
	fmt.Println("Cadena que es recivida: ",palabra)
	fmt.Println("Estado final",auxiliar_estado)

	var estado_r_a bool
	estado_r_a =estado_exitoso(auxiliar_estado, estados_de_aceptacion, estados_de_aceptacion_auxiliar)

	if estado_r_a==true{
		fmt.Println("Estado final: Aceptado")
	}else{
		fmt.Println("Estado final: Rechazado")
	}
}
