# Automatas

Para poder iniciar el programa del aFD (Automata finito determinista) hay que ejecutar con


	go run aFD.go 

en seguida se reliza la lectura del archivo 

	definition.txt 

que contine información del autómata, es decir, sus estados, estado de aceptación, estado inicial, función de transición y alfabeto.
Se pueden generar archivos a partir de la página http://ivanzuzak.info/noam/webapps/fsm_simulator/

una vez ejecutado el programa, se pide como entrada una cadena y responde si la acepta o no.

Para ejecutar el programa de nAF (Automata finito no determinista) hay que ejecutar con

	go run nAF.go

al igual que en el caso anterior se pide que se suministre la cadena de entrada y se lee el archico

	definition.txt

