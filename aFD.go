package main

import(
	"fmt"
	"bufio"
	"os"
	s "strings"
)

func main(){

	//Estructura del automata
	estados := make(map[string]int)
	var estado_inicial string
	estados_de_aceptacion := make(map[string]bool)
	alfabeto := make(map[string]bool)
	ft := make(map[string]string)

	var p1 []string

	//Se abre el archivo
	archivo, error :=os.Open("definition.txt")
	if error!=nil{
		fmt.Println("Existe un error")
	}

	//El archivo se coloca en un buffer para ser escaneado
	scanner :=bufio.NewScanner(archivo)
	var i int
	var aux string


	for scanner.Scan(){

		i++

		linea:= scanner.Text()
		//Selecciona cada estado
		if linea == "#states"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#initial"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#accepting"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#alphabet"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else if  linea == "#transitions"{
			aux = linea
			scanner.Scan()
			linea = scanner.Text()
		}else{
		}


		//Llena los parametros dependiento de la etapa
		if aux=="#states"{
			//fmt.Println(aux,linea)
			estados[linea] = 1
		}else if aux =="#initial"{
			//fmt.Println(aux,linea)
			estado_inicial = linea
		}else if aux == "#accepting"{
			//fmt.Println(aux,linea)
			estados_de_aceptacion[linea]=true
		}else if aux =="#alphabet"{
			//fmt.Println(aux,linea)
			alfabeto[linea] = true
		}else if aux =="#transitions"{
			//fmt.Println(aux,linea)
			p1 = s.Split(linea,">")
			ft[p1[0]] = p1[1]
		}
		//fmt.Println("linea",i,linea)
	}

	//Imprime en pantalla
	fmt.Println(" Estados")
	fmt.Println(estados)
	fmt.Println(" ")
	fmt.Println(" Estado inicial ")
	fmt.Println(estado_inicial)
	fmt.Println(" ")
	fmt.Println(" Estados de aceptacion ")
	fmt.Println(estados_de_aceptacion)
	fmt.Println(" ")
	fmt.Println(" Alfabeto ")
	fmt.Println(alfabeto)
	fmt.Println(" ")
	fmt.Println(" Transiciones ")
	fmt.Println(ft)

	fmt.Println("Pruebas para medidas de cadenas")

	//Lectura de la cadena
	var palabra string
	//palabra := "abcabcbabcabcba"
	fmt.Scanf("%s", &palabra)
	array_pal := s.Split(palabra, "")
	var auxiliar_estado string
	auxiliar_estado=ft[estado_inicial+":"+array_pal[0]]

	for i=1;i<len(array_pal);i++{
		auxiliar_estado=ft[auxiliar_estado+":"+array_pal[i]]
	}
	fmt.Println(auxiliar_estado)
	var resultado string
	if estados_de_aceptacion[auxiliar_estado]==true{
		resultado = "Aceptado"
	}else{
		resultado = "Rechazado"
	}
	fmt.Println("Estado del sistema: "+resultado)

}
